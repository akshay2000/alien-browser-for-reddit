import os

APP_NAME = "Alien Browser"

SUBMISSIONS = "Submissions"
COMMENTS = "Comments"

SLOT_SUBREDDIT = "Subreddit"

ATTR_CURRENT_SUBREDDIT = "CurrentSubreddit"
ATTR_BROWSING = "Browsing"
ATTR_LAST_COMMENT_ID = "LastCommentId"
ATTR_LAST_SUBMISSION_ID = "LastSubmissionId"
ATTR_YES_CONTEXT = "YesContext"
ATTR_NO_CONTEXT = "NoContext"
ATTR_ONLY_POSITIVE = "OnlyPositive"

CONTEXT_BROWSE_COMMENTS = "BrowseComments"
CONTEXT_BROWSE_SUBMISSIONS = "BrowseSubmissions"

SENT_POSITIVE = "POSITIVE"
SENT_NEGATIVE = "NEGATIVE"
SENT_NEUTRAL = "NEUTRAL"

CORS_PROXY = os.getenv("CORS_PROXY", "https://tmg5hzfsmi.execute-api.us-east-1.amazonaws.com/default/corsProxy")
