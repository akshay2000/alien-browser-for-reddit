import json

import alexareddit

launch = """{
  "version": "1.0",
  "session": {
    "new": true,
    "sessionId": "amzn1.echo-api.session.957f4417-6f86-4a85-a66b-fe3b9245f5bd",
    "application": {
      "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
    },
    "user": {
      "userId": "amzn1.ask.account.AFJNXHUJJFI7KTLKZT2Z37Y6WFAQYAM6MYJTASQKQTNHWPNJY7UDDOV6OG2KEKDP3MT7PJHW6FNEX26EQW2UR6QBUSEVG2BOWWSKRQPAGJ7A572GDPES5HONZI53XCZEOGHKY4LSLVSKIRWZWLQR7WBM5CM3KNYZKHYYBIFDNXPI4LBD5S27G6MGSWLFFZ24J3EUK3ENESAIPZQ"
    }
  },
  "context": {
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
      },
      "user": {
        "userId": "amzn1.ask.account.AFJNXHUJJFI7KTLKZT2Z37Y6WFAQYAM6MYJTASQKQTNHWPNJY7UDDOV6OG2KEKDP3MT7PJHW6FNEX26EQW2UR6QBUSEVG2BOWWSKRQPAGJ7A572GDPES5HONZI53XCZEOGHKY4LSLVSKIRWZWLQR7WBM5CM3KNYZKHYYBIFDNXPI4LBD5S27G6MGSWLFFZ24J3EUK3ENESAIPZQ"
      },
       "device": {
         "deviceId": "[DeviceId]",
         "supportedInterfaces": {
           "AudioPlayer": {},
             "Alexa.Presentation.APL": {
                 "runtime": {
                     "maxVersion": "1.0"
                 }
             }
         }
      },
      "apiEndpoint": "https://api.eu.amazonalexa.com",
      "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjE0YWUzOGJiLWU0NDctNDMyNS1hM2E0LWYwY2JhYjIyMjFmNiIsImV4cCI6MTUzOTg2NzYwMywiaWF0IjoxNTM5ODY0MDAzLCJuYmYiOjE1Mzk4NjQwMDMsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUZMVjUyT1JPMlZBN0ZINE1ESjI1QTVaUFpNVEFNRE1MTVpOQU9ZSFFNUFpNQk5SVVFSM05KN01IRlRFRkFKRUdFMzRENUhMWjMyNUJFREdUSUpWVFJLWVBFTE9OV1M2SkdMN0hRWU1OWFE1NlhZV0tRMk1PVFFPUkxQTDVGQVZNV0tERUVJS1BCN0ZDRFVTNk9XRk9YQ1dDM0hBIiwidXNlcklkIjoiYW16bjEuYXNrLmFjY291bnQuQUZKTlhIVUpKRkk3S1RMS1pUMlozN1k2V0ZBUVlBTTZNWUpUQVNRS1FUTkhXUE5KWTdVRERPVjZPRzJLRUtEUDNNVDdQSkhXNkZORVgyNkVRVzJVUjZRQlVTRVZHMkJPV1dTS1JRUEFHSjdBNTcyR0RQRVM1SE9OWkk1M1hDWkVPR0hLWTRMU0xWU0tJUldaV0xRUjdXQk01Q00zS05ZWktIWVlCSUZETlhQSTRMQkQ1UzI3RzZNR1NXTEZGWjI0SjNFVUszRU5FU0FJUFpRIn19.cWOAfWlEdcl2YENb5HoVBRifD4YPsaTWBf9jM1GMjbfB0Qj3Kl_VC0V7vVRbLfJj_1qoAXt5LtAiXoS-aO0BGP6sE4d30wgHEpgahUI-NBFD0zdVtr5AY0MupUPFdZMLQDxJv-13rcGpLvUam_3d0UeurpTpwJKCB8MrUB8TPTZsy7VQfOIQWaUqNbwsD-wN_wopDP9K5a0STiyh-HygbxYsLT-SnU6vrWhXjU1xcirSxl4nUF4Fg-JjK3actoeVn-Ryq1pxKIioB1atbBif3Ff77FoHlRYGia0GeNtvi9Xw5hAN3XtCUC3HKce71UUe8pi-3bGG4yyEDKgzO3m-GQ"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "amzn1.echo-api.request.bfa53183-28d1-47a7-8185-6bc7049ccc79",
    "timestamp": "2018-10-18T12:00:03Z",
    "locale": "en-US",
    "intent": {
      "name": "BrowseSubreddit",
      "confirmationStatus": "NONE",
      "slots": {
        "Subreddit": {
          "name": "Subreddit",
          "value": "adviceanimals",
          "confirmationStatus": "NONE",
          "source": "USER"
        }
      }
    },
    "dialogState": "STARTED"
  }
}"""

yes = """{
  "version": "1.0",
  "session": {
    "new": false,
    "sessionId": "amzn1.echo-api.session.405ba63e-7ef2-4f51-8547-ca4b1487cc90",
    "application": {
      "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
    },
    "attributes": {
      "NoContext": "BrowseSubmissions",
      "CurrentSubreddit": "askreddit",
      "Browsing": "Submissions",
      "YesContext": "BrowseComments",
      "LastCommentId": null,
      "LastSubmissionId": "9podgz"
    },
    "user": {
      "userId": "amzn1.ask.account.AENCVFHAN35UV235CEXIZZ6Q5LD56NC4CS73ALSBJIGGBBFY6BAF5B7JWZYZ4MYAYUPTQIIBODGJCA7FGQDPVQFDW4PVTRNSBEAQTOOQQIETK5BWSAKXYROBFZPMV6OEKZTOORN3I6HAZKNG5QQTBLUOXUR3UQO4JNZG6GF2U2IQOTD4HEMJG7YQF7TRNMXMFXDD3AXHTFABZ3Y"
    }
  },
  "context": {
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
      },
      "user": {
        "userId": "amzn1.ask.account.AENCVFHAN35UV235CEXIZZ6Q5LD56NC4CS73ALSBJIGGBBFY6BAF5B7JWZYZ4MYAYUPTQIIBODGJCA7FGQDPVQFDW4PVTRNSBEAQTOOQQIETK5BWSAKXYROBFZPMV6OEKZTOORN3I6HAZKNG5QQTBLUOXUR3UQO4JNZG6GF2U2IQOTD4HEMJG7YQF7TRNMXMFXDD3AXHTFABZ3Y"
      },
      "device": {
        "deviceId": "amzn1.ask.device.AHIZKEKCODUCJFYAXHSQJMIHHSU4MIFMGSVDXXEJ5RGTTKW26WJVAQPACGAQG46QCEN4SI2SFMW5CUJCHRQKMTNIMUWWRWHIGNJXQC3JJYEC76Y2IHBL7QZGXL7IFT6ZWEWDCD72NI3TIKEIQWIKIRYKFZ3A",
        "supportedInterfaces": {}
      },
      "apiEndpoint": "https://api.eu.amazonalexa.com",
      "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjE0YWUzOGJiLWU0NDctNDMyNS1hM2E0LWYwY2JhYjIyMjFmNiIsImV4cCI6MTU0MDAzMTA1NiwiaWF0IjoxNTQwMDI3NDU2LCJuYmYiOjE1NDAwMjc0NTYsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUhJWktFS0NPRFVDSkZZQVhIU1FKTUlISFNVNE1JRk1HU1ZEWFhFSjVSR1RUS1cyNldKVkFRUEFDR0FRRzQ2UUNFTjRTSTJTRk1XNUNVSkNIUlFLTVROSU1VV1dSV0hJR05KWFFDM0pKWUVDNzZZMklIQkw3UVpHWEw3SUZUNlpXRVdEQ0Q3Mk5JM1RJS0VJUVdJS0lSWUtGWjNBIiwidXNlcklkIjoiYW16bjEuYXNrLmFjY291bnQuQUVOQ1ZGSEFOMzVVVjIzNUNFWElaWjZRNUxENTZOQzRDUzczQUxTQkpJR0dCQkZZNkJBRjVCN0pXWllaNE1ZQVlVUFRRSUlCT0RHSkNBN0ZHUURQVlFGRFc0UFZUUk5TQkVBUVRPT1FRSUVUSzVCV1NBS1hZUk9CRlpQTVY2T0VLWlRPT1JOM0k2SEFaS05HNVFRVEJMVU9YVVIzVVFPNEpOWkc2R0YyVTJJUU9URDRIRU1KRzdZUUY3VFJOTVhNRlhERDNBWEhURkFCWjNZIn19.BELaMVsRQOrCTntQ6u952Wj3_bFtop0H2V2cjt_KSXp9A10aGsJdWcmMYlPDlOICTwp85R6Huv3QdILjGPnSzC3xZaeBbM6y4jz3qEm8jHLOymWEFy4aAkB-3i0vW3ZmaRBVY7J7es-vYKxuCSZamsrghBn32uDrTzqKpcnXsYf4qymvPvKymDfe9KFk6ReOByt4X9CQUu9ZdGriHd0g-nHSsv0orW3E-9MC9fdzzZ47jzSLV581cIiWQjOPSOv-fJsUe7ZnliARQ34uU8GMGk9nODX-qrAVWAhhpKDwUU43FBmfETW7QkUox4qLkAJ23DYog7mrJXalEpHDMG01qw"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "amzn1.echo-api.request.84051501-35f6-4162-8a33-101d17f7c97a",
    "timestamp": "2018-10-20T09:24:16Z",
    "locale": "en-US",
    "intent": {
      "name": "AMAZON.YesIntent",
      "confirmationStatus": "NONE"
    },
    "dialogState": "STARTED"
  }
}"""

browse = """{
	"version": "1.0",
	"session": {
		"new": false,
		"sessionId": "amzn1.echo-api.session.c0f5f29b-d718-4f14-831b-d63b248958ac",
		"application": {
			"applicationId": "amzn1.ask.skill.312203c9-5e1e-4576-a0ea-08e938602656"
		},
		"attributes": {
			"NoContext": "BrowseSubmissions",
			"CurrentSubreddit": "nosleep",
			"Browsing": "Submissions",
			"YesContext": "BrowseComments",
			"LastCommentId": null,
			"LastSubmissionId": "ay30hs"
		},
		"user": {
			"userId": "amzn1.ask.account.AHVXJJANLTKCMSJ446MZPYS37AHKAQWZDO25ZQAMS6NYEUTLMLWUDSZZBKZCHAJBCNFN3HBO6IWOSYQX35ABCYZUU5PGTGB4FM2OQ2DT75DNIK4WP5ULICFAP74FTHULMRN5DUWWGCSQDE67AO6FB6MGOUHQAFCO47IGEJ2ZEGCF23VQGHWD36J3OIA4L63WJYDH2J7MPNG6PII"
		}
	},
	"context": {
		"Alexa.Presentation.APL": {
			"token": "text_document",
			"version": "ARCRenderer-1.0",
			"componentsVisibleOnScreen": [
				{
					"id": "viewport",
					"position": "1024x600+0+0:0",
					"type": "mixed",
					"tags": {
						"viewport": {}
					},
					"visibility": 1,
					"entities": []
				}
			]
		},
		"System": {
			"application": {
				"applicationId": "amzn1.ask.skill.312203c9-5e1e-4576-a0ea-08e938602656"
			},
			"user": {
				"userId": "amzn1.ask.account.AHVXJJANLTKCMSJ446MZPYS37AHKAQWZDO25ZQAMS6NYEUTLMLWUDSZZBKZCHAJBCNFN3HBO6IWOSYQX35ABCYZUU5PGTGB4FM2OQ2DT75DNIK4WP5ULICFAP74FTHULMRN5DUWWGCSQDE67AO6FB6MGOUHQAFCO47IGEJ2ZEGCF23VQGHWD36J3OIA4L63WJYDH2J7MPNG6PII"
			},
			"device": {
				"deviceId": "amzn1.ask.device.AGF5DP4CUAU2LP3HF3BHZNCYPPR3JZKZUDCF7XH3D4HIFXLQCWD3ZU3ZAIKDDC5HETB6S5EHW2DGSFKW2ZFFJOSUJU7AXSXGPVGWYJMIHMMFEFER5TP4E6ADXD7N2O25NRJIDVV2ANWUMLM4UIEDD6ARKH4Q",
				"supportedInterfaces": {
					"Alexa.Presentation.APL": {
						"runtime": {
							"maxVersion": "1.0"
						}
					}
				}
			},
			"apiEndpoint": "https://api.eu.amazonalexa.com",
			"apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjMxMjIwM2M5LTVlMWUtNDU3Ni1hMGVhLTA4ZTkzODYwMjY1NiIsImV4cCI6MTU1MTk2NTY4MiwiaWF0IjoxNTUxOTY1MzgyLCJuYmYiOjE1NTE5NjUzODIsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUdGNURQNENVQVUyTFAzSEYzQkhaTkNZUFBSM0paS1pVRENGN1hIM0Q0SElGWExRQ1dEM1pVM1pBSUtEREM1SEVUQjZTNUVIVzJER1NGS1cyWkZGSk9TVUpVN0FYU1hHUFZHV1lKTUlITU1GRUZFUjVUUDRFNkFEWEQ3TjJPMjVOUkpJRFZWMkFOV1VNTE00VUlFREQ2QVJLSDRRIiwidXNlcklkIjoiYW16bjEuYXNrLmFjY291bnQuQUhWWEpKQU5MVEtDTVNKNDQ2TVpQWVMzN0FIS0FRV1pETzI1WlFBTVM2TllFVVRMTUxXVURTWlpCS1pDSEFKQkNORk4zSEJPNklXT1NZUVgzNUFCQ1laVVU1UEdUR0I0Rk0yT1EyRFQ3NUROSUs0V1A1VUxJQ0ZBUDc0RlRIVUxNUk41RFVXV0dDU1FERTY3QU82RkI2TUdPVUhRQUZDTzQ3SUdFSjJaRUdDRjIzVlFHSFdEMzZKM09JQTRMNjNXSllESDJKN01QTkc2UElJIn19.KExnDa8uq_L9kWL1rcNmGt1mLNW6a36MC3FRrm9AgdpRQZSwpNHEV9gLTn2HQAGuBASiheggTWjTze8_OeiTS3SQjzgZPN-lxyrmPOvVVLYQeyj9asGJToqEvCE4gLyLL_zqd4mpFwmK7YJ6EkpzKQo-r2qO4obcY2TnvhZmlA6fx50BcDjZx7opM5Y0JZpv1p2eGBhTDJnX2VxW8tdCIQ-elgC5BnOWAnqbWZLC2lQDGy_Hmw-DOSLC9_BbD3TCkDd6j8puKBzMLbhnAaOEC8MLS1qgsp0-pndH9EbY2AQtMx5qbzgUJMOI7L0OCtUpRfoYP7uFcUJOc5Ie4Tlr8Q"
		},
		"Viewport": {
			"experiences": [
				{
					"arcMinuteWidth": 246,
					"arcMinuteHeight": 144,
					"canRotate": false,
					"canResize": false
				}
			],
			"shape": "RECTANGLE",
			"pixelWidth": 1024,
			"pixelHeight": 600,
			"dpi": 160,
			"currentPixelWidth": 1024,
			"currentPixelHeight": 600,
			"touch": [
				"SINGLE"
			]
		}
	},
	"request": {
		"type": "IntentRequest",
		"requestId": "amzn1.echo-api.request.c8529b19-6662-4eca-b8e3-0664faeca891",
		"timestamp": "2019-03-07T13:29:42Z",
		"locale": "en-IN",
		"intent": {
			"name": "BrowseSubreddit",
			"confirmationStatus": "NONE",
			"slots": {
				"Subreddit": {
					"name": "Subreddit",
					"value": "nosleep",
					"resolutions": {
						"resolutionsPerAuthority": [
							{
								"authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.312203c9-5e1e-4576-a0ea-08e938602656.SubredditName",
								"status": {
									"code": "ER_SUCCESS_MATCH"
								},
								"values": [
									{
										"value": {
											"name": "nosleep",
											"id": "cbb49773aef9df81be77632b2623e92f"
										}
									}
								]
							}
						]
					},
					"confirmationStatus": "NONE",
					"source": "USER"
				}
			}
		},
		"dialogState": "STARTED"
	}
}"""

browse_slot = """
{
  "version": "1.0",
  "session": {
    "new": false,
    "sessionId": "amzn1.echo-api.session.e06e500d-4e37-4540-84b2-36d92bcd24c2",
    "application": {
      "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
    },
    "user": {
      "userId": "amzn1.ask.account.AENCVFHAN35UV235CEXIZZ6Q5LD56NC4CS73ALSBJIGGBBFY6BAF5B7JWZYZ4MYAYUPTQIIBODGJCA7FGQDPVQFDW4PVTRNSBEAQTOOQQIETK5BWSAKXYROBFZPMV6OEKZTOORN3I6HAZKNG5QQTBLUOXUR3UQO4JNZG6GF2U2IQOTD4HEMJG7YQF7TRNMXMFXDD3AXHTFABZ3Y"
    }
  },
  "context": {
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6"
      },
      "user": {
        "userId": "amzn1.ask.account.AENCVFHAN35UV235CEXIZZ6Q5LD56NC4CS73ALSBJIGGBBFY6BAF5B7JWZYZ4MYAYUPTQIIBODGJCA7FGQDPVQFDW4PVTRNSBEAQTOOQQIETK5BWSAKXYROBFZPMV6OEKZTOORN3I6HAZKNG5QQTBLUOXUR3UQO4JNZG6GF2U2IQOTD4HEMJG7YQF7TRNMXMFXDD3AXHTFABZ3Y"
      },
      "device": {
        "deviceId": "amzn1.ask.device.AHIZKEKCODUCJFYAXHSQJMIHHSU4MIFMGSVDXXEJ5RGTTKW26WJVAQPACGAQG46QCEN4SI2SFMW5CUJCHRQKMTNIMUWWRWHIGNJXQC3JJYEC76Y2IHBL7QZGXL7IFT6ZWEWDCD72NI3TIKEIQWIKIRYKFZ3A",
        "supportedInterfaces": {}
      },
      "apiEndpoint": "https://api.eu.amazonalexa.com",
      "apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjE0YWUzOGJiLWU0NDctNDMyNS1hM2E0LWYwY2JhYjIyMjFmNiIsImV4cCI6MTU0MDA0Nzg2MSwiaWF0IjoxNTQwMDQ0MjYxLCJuYmYiOjE1NDAwNDQyNjEsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUhJWktFS0NPRFVDSkZZQVhIU1FKTUlISFNVNE1JRk1HU1ZEWFhFSjVSR1RUS1cyNldKVkFRUEFDR0FRRzQ2UUNFTjRTSTJTRk1XNUNVSkNIUlFLTVROSU1VV1dSV0hJR05KWFFDM0pKWUVDNzZZMklIQkw3UVpHWEw3SUZUNlpXRVdEQ0Q3Mk5JM1RJS0VJUVdJS0lSWUtGWjNBIiwidXNlcklkIjoiYW16bjEuYXNrLmFjY291bnQuQUVOQ1ZGSEFOMzVVVjIzNUNFWElaWjZRNUxENTZOQzRDUzczQUxTQkpJR0dCQkZZNkJBRjVCN0pXWllaNE1ZQVlVUFRRSUlCT0RHSkNBN0ZHUURQVlFGRFc0UFZUUk5TQkVBUVRPT1FRSUVUSzVCV1NBS1hZUk9CRlpQTVY2T0VLWlRPT1JOM0k2SEFaS05HNVFRVEJMVU9YVVIzVVFPNEpOWkc2R0YyVTJJUU9URDRIRU1KRzdZUUY3VFJOTVhNRlhERDNBWEhURkFCWjNZIn19.MTF9XYeaFeX9NWR5GBgvb6xuv6CTgsvz_YycRje_Igatsi2V43AkQK8RIeaFjhxl5iIxOJQjlqi6jYnvS1rTBk6MGfoQxjHNC1vikk8Qt199iCGqt0uotUxrGubrOm73zYy6T3PCcqkqq3MeWyhoaRKtvDE7sSKvAI6I-d88d_eCw6X7BcZSln28cF-5RtpCImGuX4jx8wZQDWfwZHoG0X1ArLB1D94hpFEQpDj_J-GfTQuUxw-LzhDB0oXKlhBnjHwwl0vH-ah0B8OD_EOnwIFgfv0ur2wLX7wp-K8fOvB5pNsN67qPNfncYc6r4IvkEJz9baVDMwaygfFmcL785w"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "amzn1.echo-api.request.f1f4e62f-42d7-4374-8389-c3fc2dc6e621",
    "timestamp": "2018-10-20T14:04:21Z",
    "locale": "en-US",
    "intent": {
      "name": "BrowseSubreddit",
      "confirmationStatus": "NONE",
      "slots": {
        "Subreddit": {
          "name": "Subreddit",
          "value": "ask me anything",
          "resolutions": {
            "resolutionsPerAuthority": [
              {
                "authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.14ae38bb-e447-4325-a3a4-f0cbab2221f6.SubredditName",
                "status": {
                  "code": "ER_SUCCESS_MATCH"
                },
                "values": [
                  {
                    "value": {
                      "name": "iama",
                      "id": "iama"
                    }
                  }
                ]
              }
            ]
          },
          "confirmationStatus": "NONE",
          "source": "USER"
        }
      }
    },
    "dialogState": "STARTED"
  }
}"""

message = json.loads(browse)
res = alexareddit.handler(message, None)
print(json.dumps(res, indent=2))

# subs = cache.get_submissions('adviceanimals')
# i = 0
# while i < 10:
#     sub = next(subs)
#     if sub.stickied:
#         continue
#     resp = utils.make_submission_response(sub)
#     i += 1

# s10 = subs[10]
# print(s10)
# s3 = subs[3]
# print(s3)
#
# coms = cache.get_comments('9pcete')
# c10 = coms[10]
# print(c10)
# c3 = coms[3]
# print(c3)
