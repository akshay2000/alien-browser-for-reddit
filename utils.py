import json
import logging
import random
from urllib import parse
from xml.sax import saxutils

import boto3
import requests
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model.interfaces.alexa.presentation.apl.render_document_directive import RenderDocumentDirective
from bs4 import BeautifulSoup
from markdown import markdown

from constants import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# region ML Utils

rekognition_client = boto3.client("rekognition", region_name="us-east-1")

comprehend_client = boto3.client("comprehend", region_name="us-east-1")


def analyze_sentiments(docs: list):
    res = []
    max_batch_size = 25
    start = 0
    while True:
        current_batch = docs[start: start + max_batch_size]
        if len(current_batch) == 0:
            break
        response = comprehend_client.batch_detect_sentiment(TextList=current_batch, LanguageCode="en")
        res.extend(response["ResultList"])
        start += max_batch_size

    sentiments = list(map(lambda i: i["Sentiment"], res))
    return sentiments


def extract_image_text(image_url):
    image_bytes = requests.get(image_url).content
    result = rekognition_client.detect_text(
        Image={"Bytes": image_bytes}
    )
    detections = result["TextDetections"]
    detected_lines = list(filter(lambda d: d["Type"] == "LINE", detections))
    lines = list(map(lambda d: d["DetectedText"].strip(), detected_lines))

    def is_watermark(line: str):
        split = line.split(".")
        if len(split) > 0:
            last = split[-1]
            return last in ["com", "in", "org"]
        return False

    if len(lines) > 0 and is_watermark(lines[-1]):
        lines.remove(lines[-1])
    text = " ".join(lines)

    return text


# endregion


__voices = ["Ivy", "Joanna", "Joey", "Justin", "Kendra", "Kimberly", "Matthew", "Salli", "Amy", "Brian", "Emma"]


def __get_voice():
    return random.choice(__voices)


# region Response Utils

__sanitizer = [
    ("asshole", "poop hole"),
    ("cunt", "kant"),
    ("fuck", "eff"),
    ("shit", "poop"),
    ("crap", "poop"),
    ("hard on", "rough on"),
    ("bitch", "beep"),
    ("tl;dr", "TLDR:"),
    ("TL;DR", "TLDR:"),
    ("Tl;dr", "TLDR:"),
    ("god damn", "god dam"),
    ("goddamn", "god dam")
]


def sanitize(text: str) -> str:
    sanitized = text
    for left, right in __sanitizer:
        sanitized = sanitized \
            .replace(left, right) \
            .replace(left.title(), right.title()) \
            .replace(left.upper(), right.upper()) \
            .replace(left.capitalize(), right.capitalize())
    return sanitized


def set_session_attribute(attr_name: str, value, handler_input):
    session_attr = handler_input.attributes_manager.session_attributes
    session_attr[attr_name] = value


def reset_session_attributes(attr_manager):
    attr_manager.session_attributes = dict()


def make_response(speech: str, should_end_session: bool,
                  handler_input: HandlerInput, directives=None):
    sanitized_speech = sanitize(speech)

    response_factory = handler_input.response_builder \
        .speak(sanitized_speech) \
        .set_should_end_session(should_end_session)

    if directives:
        if isinstance(directives, list):
            for d in directives:
                response_factory.add_directive(d)
        else:
            response_factory.add_directive(directives)

    return response_factory.response


def make_comment_response(comment, handler_input, prefix: str = None):
    author = comment.author.name
    body = comment.body
    voice = __get_voice()
    speech = f"{author} says, <voice name=\"{voice}\"> {saxutils.escape(body)} </voice>"
    speech = f"{speech} <break time=\"750ms\"/> Would you like to browse next comment?"
    if prefix:
        speech = f"{prefix} {speech}"

    if is_apl_supported(handler_input):
        directive = build_text_directive(author, body)
    else:
        directive = None

    set_session_attribute(ATTR_BROWSING, COMMENTS, handler_input)
    set_session_attribute(ATTR_LAST_COMMENT_ID, comment.id, handler_input)
    set_session_attribute(ATTR_YES_CONTEXT, CONTEXT_BROWSE_COMMENTS, handler_input)
    set_session_attribute(ATTR_NO_CONTEXT, CONTEXT_BROWSE_SUBMISSIONS, handler_input)

    return make_response(speech, False, handler_input, directive)


MAX_CONTENT_LENGTH = 7000


def truncate_text(text: str, max_length: int):
    sentences = text.split(".")
    output = ""
    for s in sentences:
        if len(output) + len(s) >= max_length:
            break
        output = output + s + "."
    return output


def markdown_to_text(text: str) -> str:
    html = markdown(text)
    # html = re.sub(r'<pre>(.*?)</pre>', ' ', html)
    # html = re.sub(r'<code>(.*?)</code >', ' ', html)
    soup = BeautifulSoup(html, "html.parser")
    out = ''.join(soup.findAll(text=True))
    return out


def make_submission_response(submission, handler_input, prefix: str = None):
    if __is_meme(submission):
        apl_supported = is_apl_supported(handler_input)
        response = __make_meme_submission_response(submission, handler_input, prefix, apl_supported)
    else:
        author = submission.author.name
        title = submission.title
        content = submission.selftext.replace("\\n", "\n")
        try:
            content = markdown_to_text(content)
        except Exception as e:
            logger.error(e)

        if len(content) >= MAX_CONTENT_LENGTH:
            content = truncate_text(content, MAX_CONTENT_LENGTH)

        voice = __get_voice()

        tell_verb = "reports" if is_news(submission) else random.choice(["says", "writes"])

        speech = f"{author} {tell_verb}, <voice name=\"{voice}\"> {saxutils.escape(title)}"
        if content:
            speech = f"{speech} <break time=\"0.5s\"/> {saxutils.escape(content)}"

        speech = f"{speech} </voice>"

        speech = f"{speech} <break time=\"750ms\"/> Would you like to browse comments?"
        if prefix:
            speech = f"{prefix} {speech}"

        if is_apl_supported(handler_input):
            directive = build_text_directive(author, title, content)
        else:
            directive = None

        response = make_response(speech, False, handler_input, directive)

    set_session_attribute(ATTR_BROWSING, SUBMISSIONS, handler_input)
    set_session_attribute(ATTR_LAST_SUBMISSION_ID, submission.id, handler_input)
    set_session_attribute(ATTR_LAST_COMMENT_ID, None, handler_input)
    set_session_attribute(ATTR_YES_CONTEXT, CONTEXT_BROWSE_COMMENTS, handler_input)
    set_session_attribute(ATTR_NO_CONTEXT, CONTEXT_BROWSE_SUBMISSIONS, handler_input)

    return response


def is_news(item) -> bool:
    if item is None:
        return False
    if isinstance(item, str):
        subreddit_name = item
    else:
        subreddit_name = item.subreddit.display_name.lower()
    return subreddit_name in ["news", "worldnews", "politics", "offbeat", "india"]


def __is_meme(submission) -> bool:
    subreddit = submission.subreddit.display_name.lower()
    return subreddit in ["adviceanimals", "memes"]


def __make_meme_submission_response(submission, handler_input, prefix: str = None, apl_supported: bool = False):
    author = submission.author.name

    image_url = submission.preview["images"][0]["source"]["url"]
    meme_text = extract_image_text(image_url)

    title = sanitize(submission.title)
    voice = __get_voice()

    speech = f"{author} says, <voice name=\"{voice}\"> {saxutils.escape(title)} </voice>"
    if meme_text:
        speech = f"{speech} <break time=\"1s\"/> Meme reads, {saxutils.escape(meme_text)}"
    speech = f"{speech} <break time=\"1s\"/> Would you like to browse comments?"
    if prefix:
        speech = f"{prefix} {speech}"

    if apl_supported:
        resolutions = submission.preview["images"][0]["resolutions"]
        resolutions.append(submission.preview["images"][0]["source"])
        directive = build_meme_directive(resolutions, title)
    else:
        directive = None

    return make_response(speech, False, handler_input, directive)


def find_closest_resolution(width_to_find: int, resolutions: list):
    tuples = list(map(lambda r: (abs(r["width"] - width_to_find), r), resolutions))
    min_difference = min(tuples, key=lambda t: t[0])
    return min_difference[1]


def find_closest_resolution_url(width_to_find: int, resolutions: list) -> str:
    return find_closest_resolution(width_to_find, resolutions)["url"]


# endregion

# region APL Utils

__logo_url = "https://images-na.ssl-images-amazon.com/images/I/51uCskig3yL._SL210_QL95_BG0,0,0,0_FMpng_.png"


def __get_background_image():
    return "https://i.imgur.com/nxeyfnY.jpg"


def __to_apl_markup(text: str):
    old_length = -1
    s = text
    while old_length != len(s):
        s = text.replace("\n\n", "\n").replace("  ", " ")
        old_length = len(s)
    return s.replace("\n", "<br>")


def build_text_directive(author: str, title: str = None, content: str = None) -> RenderDocumentDirective:
    data_source = __build_text_data_source(author, title, content)
    text_document = __get_document("text")
    directive = RenderDocumentDirective(token="text_document", datasources=data_source, document=text_document)

    return directive


def __build_text_data_source(author: str, title: str, content: str) -> dict:
    if title and content:
        apl_title = title
        apl_content = content
    else:
        apl_title = author
        apl_content = title or content
    subreddits = ["Ask Me Anything", "Jokes", "Ask Reddit", "Get Motivated", "Advice Animals", "Dad Jokes",
                  "Technology", "Explain like I'm Five", "Stories"]

    hint = "Try saying \"browse {0}\"".format(random.choice(subreddits))
    data_source = {
        "data": {
            "title": __to_apl_markup(sanitize(apl_title)),
            "content": __to_apl_markup(sanitize(apl_content)),
            "background": __get_background_image(),
            "logoUrl": __logo_url,
            "hint": hint
        }
    }
    if author:
        data_source["data"]["subtitle"] = sanitize(author)

    return data_source


def build_meme_directive(images: list, post_title: str) -> RenderDocumentDirective:
    data_source = __build_image_data_source(images, post_title)
    meme_document = __get_document("meme")
    directive = RenderDocumentDirective(token="meme_document", document=meme_document, datasources=data_source)
    return directive


def __build_image_data_source(images: list, post_title: str):
    small_image_width = 380
    medium_image_width = 921
    large_image_width = 1152
    extra_large_image_width = 860

    data_source = {
        "data": {
            "title": post_title,
            "image": {
                "small": __build_image(images, small_image_width),
                "medium": __build_image(images, medium_image_width),
                "large": __build_image(images, large_image_width),
                "extraLarge": __build_image(images, extra_large_image_width),
            },
            "logoUrl": __logo_url,
            "background": __get_background_image()
        }
    }
    return data_source


def __build_image(images: list, desired_width: int):
    closest_image = find_closest_resolution(desired_width, images)
    image_url = closest_image["url"]
    headers = requests.head(image_url).headers
    cors_header_value = headers.get("Access-Control-Allow-Origin")
    if not cors_header_value or cors_header_value != "*" or cors_header_value != "*.amazon.com":
        encoded_url = parse.quote_plus(image_url)
        url = f"{CORS_PROXY}?url={encoded_url}"
    else:
        url = image_url

    in_height = closest_image["height"]
    in_width = closest_image["width"]
    desired_height = (in_height * desired_width) / in_width

    return {
        "url": url,
        "width": desired_width,
        "height": desired_height
    }


__documents = None


def __get_document(name: str):
    global __documents
    if not __documents:
        with open("documents.json") as f:
            docs_text = f.read()
            __documents = json.loads(docs_text)
    return __documents[name]


def is_apl_supported(handler_input: HandlerInput):
    context = handler_input.request_envelope.context
    if not context:
        return False
    device = context.system.device
    if not device:
        return False
    if device.supported_interfaces.alexa_presentation_apl:
        return True
    return False

# endregion
