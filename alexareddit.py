from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_model import Response
from ask_sdk_model.ui import SimpleCard

import cache
from utils import *

sb = SkillBuilder()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_slot(request_envelope, slot_name, default_value=None):
    slots = request_envelope.request.intent.slots
    slot = slots[slot_name]
    if not slot:
        return default_value
    if slot.resolutions:
        values = slot.resolutions.resolutions_per_authority[0].values
        if not values:
            return slot.value
        if values[0].value.name:
            return values[0].value.name
        else:
            return slot.value
    else:
        return slot.value


def get_session_attribute(attr_name: str, handler_input, default=None):
    session_attr = handler_input.attributes_manager.session_attributes
    value = session_attr.get(attr_name)
    return value if value else default


@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    # type: (HandlerInput) -> Response
    """Handler for Skill Launch."""
    speech_text = "Which subreddit would you like to browse?"

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("What's Up?", speech_text)).set_should_end_session(
        False).response


@sb.request_handler(can_handle_func=is_intent_name("BrowseSubreddit"))
def browse_subreddit_intent_handler(handler_input):
    reset_session_attributes(handler_input.attributes_manager)

    subreddit_name = get_slot(handler_input.request_envelope, SLOT_SUBREDDIT).replace(' ', '').replace('.', '')
    submissions = cache.get_submissions(subreddit_name)
    submission = next(s for s in submissions if not s.stickied and not s.over_18)

    set_session_attribute(ATTR_CURRENT_SUBREDDIT, subreddit_name, handler_input)

    prefix = random.choice(["Here's subreddit {}.", "Here is subreddit {} for you!", "Browsing subreddit {} now."])
    prefix = prefix.format(subreddit_name)
    return make_submission_response(submission, handler_input, prefix)


@sb.request_handler(can_handle_func=is_intent_name("BrowseComments"))
def browse_comments_intent_handler(handler_input):
    submission_id = get_session_attribute(ATTR_LAST_SUBMISSION_ID, handler_input)
    if not submission_id:
        speech = "Sorry, you must be browsing a post to browse comments!"
        return make_response(speech, False, handler_input)
    else:
        comment = cache.get_next_comment(None, submission_id)
        return make_comment_response(comment, handler_input)


@sb.request_handler(can_handle_func=is_intent_name("OnlyBrowsePositive"))
def only_browse_positive_intent_handler(handler_input):
    subreddit_name = get_session_attribute(ATTR_CURRENT_SUBREDDIT, handler_input)
    last_submission_id = get_session_attribute(ATTR_LAST_SUBMISSION_ID, handler_input)

    if not is_news(subreddit_name):
        subreddit_name = "news"
        last_submission_id = None
        set_session_attribute(ATTR_CURRENT_SUBREDDIT, subreddit_name, handler_input)
        set_session_attribute(ATTR_LAST_SUBMISSION_ID, last_submission_id, handler_input)

    okay_word = random.choice(["Alright", "Okay"])
    prefix = f"{okay_word}, showing positive posts from {subreddit_name}."
    set_session_attribute(ATTR_ONLY_POSITIVE, "True", handler_input)
    submission = cache.get_next_positive_submission(last_submission_id, subreddit_name)

    return make_submission_response(submission, handler_input, prefix)


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.YesIntent"))
def yes_intent_handler(handler_input):
    context = get_session_attribute(ATTR_YES_CONTEXT, handler_input)
    return handle_context(handler_input, context, True)


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.NoIntent"))
def no_intent_handler(handler_input):
    context = get_session_attribute(ATTR_NO_CONTEXT, handler_input)
    return handle_context(handler_input, context, False)


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.NextIntent"))
def next_intent_handler(handler_input):
    currently_browsing = get_session_attribute(ATTR_BROWSING, handler_input)
    if currently_browsing is None:
        return make_response("Sorry, nothing is being browsed.", True, handler_input)
    if currently_browsing == COMMENTS:
        context = CONTEXT_BROWSE_COMMENTS
    elif currently_browsing == SUBMISSIONS:
        context = CONTEXT_BROWSE_SUBMISSIONS
    else:
        raise ValueError(f"Don't know how to handle browsing {currently_browsing}.")
    return handle_context(handler_input, context, True)


def handle_context(handler_input, context: str, is_yes: bool):
    if context == CONTEXT_BROWSE_COMMENTS:
        last_comment_id = get_session_attribute(ATTR_LAST_COMMENT_ID, handler_input)
        submission_id = get_session_attribute(ATTR_LAST_SUBMISSION_ID, handler_input)
        comment = cache.get_next_comment(last_comment_id, submission_id)
        return make_comment_response(comment, handler_input)
    elif context == CONTEXT_BROWSE_SUBMISSIONS:
        last_submission_id = get_session_attribute(ATTR_LAST_SUBMISSION_ID, handler_input)
        subreddit_name = get_session_attribute(ATTR_CURRENT_SUBREDDIT, handler_input)
        only_positive = get_session_attribute(ATTR_ONLY_POSITIVE, handler_input)
        only_positive = only_positive and only_positive == "True"
        if is_news(subreddit_name) and only_positive:
            submission = cache.get_next_positive_submission(last_submission_id, subreddit_name)
        else:
            submission = cache.get_next_submission(last_submission_id, subreddit_name)

        okay_word = random.choice(["Alright", "Okay"])
        prefix = f"{okay_word}, here's the next post!" if not is_yes else None
        return make_submission_response(submission, handler_input, prefix)
    else:
        raise ValueError(f"Don't know how to handle context {context}.")


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    # type: (HandlerInput) -> Response
    """Handler for Help Intent."""
    speech_text = """You can say "Browse AskReddit" and I'll start reading the posts out."""

    return handler_input.response_builder.speak(speech_text).ask(
        speech_text).set_card(SimpleCard(f"{APP_NAME} Help", speech_text)).response


@sb.request_handler(
    can_handle_func=lambda handler_input:
    is_intent_name("AMAZON.CancelIntent")(handler_input) or
    is_intent_name("AMAZON.StopIntent")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
    # type: (HandlerInput) -> Response
    """Single handler for Cancel and Stop Intent."""
    speech_text = random.choice(["Goodbye!", "See you later, alligator", "See you in a while, crocodile", "Tata",
                                 "Don't forget to rate us on Alexa app for more awesome content!"])

    return handler_input.response_builder.speak(speech_text).set_card(
        SimpleCard("Bye!", speech_text)).set_should_end_session(True).response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    # type: (HandlerInput) -> Response
    """AMAZON.FallbackIntent is only available in en-US locale.
    This handler will not be triggered except in that locale,
    so it is safe to deploy on any locale.
    """
    speech = (
        f"The {APP_NAME} skill can't help you with that.  "
        "You can say Browse AskReddit")
    reprompt = "You can say Browse AskReddit"
    handler_input.response_builder.speak(speech).ask(reprompt)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
    # type: (HandlerInput) -> Response
    """Handler for Session End."""
    return handler_input.response_builder.response


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    # type: (HandlerInput, Exception) -> Response
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    logger.error(exception, exc_info=True)

    speech = "Sorry, there was some problem. Please try again!"
    handler_input.response_builder.speak(speech).ask(speech)

    return handler_input.response_builder.response


handler = sb.lambda_handler()
