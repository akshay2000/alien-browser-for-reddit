from os import getenv

import praw
from praw.models import Subreddit, Submission

import utils
from constants import SENT_NEGATIVE

__subreddits_cache = {}
__submissions_cache = {}
__comments_cache = {}
__sentiment_cache = {}

reddit = praw.Reddit(client_id=getenv("CLIENT_ID"),
                     client_secret=getenv("CLIENT_SECRET"),
                     user_agent=getenv("USER_AGENT"))


def get_next_positive_submission(submission_id, subreddit_name) -> Submission:
    current_sub = get_next_submission(submission_id, subreddit_name)
    while get_sentiment(current_sub) == SENT_NEGATIVE:
        current_sub = get_next_submission(current_sub.id, subreddit_name)
    return current_sub


def get_sentiment(submission: Submission) -> str:
    submission_id = submission.id
    if submission_id not in __sentiment_cache:
        batch_size = 20
        subs = [submission]
        docs = [submission.title]
        current_submission_id = submission_id
        for i in range(batch_size):
            next_sub = get_next_submission(current_submission_id, submission.subreddit.display_name)
            current_submission_id = next_sub.id
            subs.append(next_sub)
            docs.append(next_sub.title)
        sentiments = utils.analyze_sentiments(docs)
        for sub, sentiment in zip(subs, sentiments):
            __sentiment_cache[sub.id] = sentiment
    return __sentiment_cache[submission_id]


def get_subreddit(subreddit_name: str) -> Subreddit:
    if subreddit_name not in __subreddits_cache:
        subreddit = reddit.subreddit(subreddit_name)
        __subreddits_cache[subreddit_name] = subreddit
    return __subreddits_cache[subreddit_name]


def get_submissions(subreddit_name: str) -> "IterWrapper":
    if subreddit_name not in __submissions_cache:
        subreddit = get_subreddit(subreddit_name)
        submissions = subreddit.hot()
        __submissions_cache[subreddit_name] = IterWrapper(submissions)
    return __submissions_cache[subreddit_name]


def get_comments(submission_id):
    if submission_id not in __comments_cache:
        submission = reddit.submission(id=submission_id)
        comments = submission.comments
        __comments_cache[submission_id] = comments
    return __comments_cache[submission_id]


def get_next_item(current_item_id, items):
    if current_item_id is None:
        return items[0]
    prev_item = None
    for i in items:
        if prev_item and prev_item.id == current_item_id:
            return i
        else:
            prev_item = i


def get_next_comment(current_comment_id, submission_id):
    comments = get_comments(submission_id)
    return get_next_item(current_comment_id, comments)


def get_next_submission(current_submission_id, subreddit_name) -> Submission:
    submissions = get_submissions(subreddit_name)
    ret = get_next_item(current_submission_id, submissions)
    while ret.over_18:
        ret = get_next_item(ret.id, submissions)
    return ret


class IterWrapper:
    def __init__(self, itr):
        self.__cached = []
        self.__itr = itr
        self.counter = 0

    def __iter__(self):
        self.counter = 0
        return self

    def __next__(self):
        if self.counter > len(self.__cached) - 1:
            ret = self.__get_from_itr()
            self.__cached.append(ret)
        else:
            ret = self.__cached[self.counter]
        self.counter += 1
        return ret

    def __getitem__(self, index):
        if index > len(self.__cached) - 1:
            while index > len(self.__cached) - 1:
                item = self.__get_from_itr()
                self.__cached.append(item)
        return self.__cached[index]

    def __get_from_itr(self):
        item = next(self.__itr)
        return item
